import random

pos_text = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ!'"
target = "Hello! This is artificial intelligence"

def evaluation(dna):
    if len(dna) != len(target):
        return 1000.0
    val = 0
    for i in range(0, len(target)):
        if target[i] != dna[i]:
            val += 1
    return val


def random_pop(num,pop):
    for i in range(0, num):
        pop.append("".join([random.choice(pos_text) for c in range(0,len(target))]))

def crossover(dna1, dna2):
    if len(dna1) != len(dna2):
        raise Exception("DNAs length are not equal")
    half = int(len(dna1)/2)
    child1 = dna1[0:half]+dna2[half:]
    child2 = dna2[0:half]+dna1[half:]
    return child1, child2

def mutation(dna):
    dna_c = dna[:]
    s_str = list(dna_c)
    index = random.randrange(0, len(s_str))
    s_str[index] = random.choice(pos_text)
    return "".join(s_str)

population = []
pop_length = 1000
elite_pop = []
random_pop(pop_length, population)


running = True
max_generations = 10000
generation = 0
while running:
    if generation >= max_generations:
        running = False
        continue
    population = sorted(population, key=lambda dna: evaluation(dna))
    while len(elite_pop)<pop_length:
        for i in range(0, pop_length-1):
            if random.random() < 0.9:
                child1, child2 = crossover(population[i], population[i+1])
                elite_pop.append(child1)
                elite_pop.append(child2)
            if random.random() < 0.5:
                elite_pop.append(mutation(population[i]))
    elite_pop = elite_pop[0:pop_length]
    population = elite_pop[:]
    elite_pop.clear()
    print(f"generation:{str(generation).zfill(2)}\t\"{population[0]}\"\tERROR:{evaluation(population[0])}")
    if evaluation(population[0]) == 0:
        running = False
        continue
        
    generation+=1